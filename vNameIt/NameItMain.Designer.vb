﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NameItMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NameItMain))
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.XclsxFileName = New System.Windows.Forms.TextBox()
        Me.RenameFileName = New System.Windows.Forms.TextBox()
        Me.StartRename = New System.Windows.Forms.Button()
        Me.OpenFileOName = New System.Windows.Forms.Button()
        Me.OpenFileReName = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Status = New System.Windows.Forms.GroupBox()
        Me.StatusBox = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.rb_Pdf = New System.Windows.Forms.RadioButton()
        Me.rb_Else = New System.Windows.Forms.RadioButton()
        Me.tb_ending = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Status.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'XclsxFileName
        '
        Me.XclsxFileName.Location = New System.Drawing.Point(6, 29)
        Me.XclsxFileName.Name = "XclsxFileName"
        Me.XclsxFileName.Size = New System.Drawing.Size(453, 27)
        Me.XclsxFileName.TabIndex = 0
        '
        'RenameFileName
        '
        Me.RenameFileName.Location = New System.Drawing.Point(6, 21)
        Me.RenameFileName.Name = "RenameFileName"
        Me.RenameFileName.Size = New System.Drawing.Size(453, 27)
        Me.RenameFileName.TabIndex = 1
        '
        'StartRename
        '
        Me.StartRename.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StartRename.Location = New System.Drawing.Point(526, 369)
        Me.StartRename.Name = "StartRename"
        Me.StartRename.Size = New System.Drawing.Size(75, 33)
        Me.StartRename.TabIndex = 2
        Me.StartRename.Text = "Start"
        Me.StartRename.UseVisualStyleBackColor = True
        '
        'OpenFileOName
        '
        Me.OpenFileOName.Location = New System.Drawing.Point(479, 27)
        Me.OpenFileOName.Name = "OpenFileOName"
        Me.OpenFileOName.Size = New System.Drawing.Size(75, 29)
        Me.OpenFileOName.TabIndex = 3
        Me.OpenFileOName.Text = "Sök"
        Me.OpenFileOName.UseVisualStyleBackColor = True
        '
        'OpenFileReName
        '
        Me.OpenFileReName.Location = New System.Drawing.Point(479, 19)
        Me.OpenFileReName.Name = "OpenFileReName"
        Me.OpenFileReName.Size = New System.Drawing.Size(75, 29)
        Me.OpenFileReName.TabIndex = 4
        Me.OpenFileReName.Text = "Sök"
        Me.OpenFileReName.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.XclsxFileName)
        Me.GroupBox1.Controls.Add(Me.OpenFileOName)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(41, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(565, 75)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Excel fil med orginalnamn"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.OpenFileReName)
        Me.GroupBox2.Controls.Add(Me.RenameFileName)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(41, 104)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(565, 64)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Filer att byta namn på "
        '
        'Status
        '
        Me.Status.Controls.Add(Me.StatusBox)
        Me.Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Status.Location = New System.Drawing.Point(47, 246)
        Me.Status.Name = "Status"
        Me.Status.Size = New System.Drawing.Size(473, 162)
        Me.Status.TabIndex = 8
        Me.Status.TabStop = False
        Me.Status.Text = "Status"
        '
        'StatusBox
        '
        Me.StatusBox.Location = New System.Drawing.Point(6, 20)
        Me.StatusBox.Multiline = True
        Me.StatusBox.Name = "StatusBox"
        Me.StatusBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.StatusBox.Size = New System.Drawing.Size(453, 136)
        Me.StatusBox.TabIndex = 0
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.tb_ending)
        Me.GroupBox3.Controls.Add(Me.rb_Else)
        Me.GroupBox3.Controls.Add(Me.rb_Pdf)
        Me.GroupBox3.Location = New System.Drawing.Point(47, 175)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(559, 65)
        Me.GroupBox3.TabIndex = 9
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Val av endelse"
        '
        'rb_Pdf
        '
        Me.rb_Pdf.AutoSize = True
        Me.rb_Pdf.Checked = True
        Me.rb_Pdf.Location = New System.Drawing.Point(7, 23)
        Me.rb_Pdf.Name = "rb_Pdf"
        Me.rb_Pdf.Size = New System.Drawing.Size(53, 21)
        Me.rb_Pdf.TabIndex = 0
        Me.rb_Pdf.TabStop = True
        Me.rb_Pdf.Text = ".pdf"
        Me.rb_Pdf.UseVisualStyleBackColor = True
        '
        'rb_Else
        '
        Me.rb_Else.AutoSize = True
        Me.rb_Else.Location = New System.Drawing.Point(79, 23)
        Me.rb_Else.Name = "rb_Else"
        Me.rb_Else.Size = New System.Drawing.Size(59, 21)
        Me.rb_Else.TabIndex = 1
        Me.rb_Else.Text = "valfri"
        Me.rb_Else.UseVisualStyleBackColor = True
        '
        'tb_ending
        '
        Me.tb_ending.Enabled = False
        Me.tb_ending.Location = New System.Drawing.Point(161, 20)
        Me.tb_ending.Name = "tb_ending"
        Me.tb_ending.Size = New System.Drawing.Size(100, 23)
        Me.tb_ending.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(267, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(227, 17)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "(om valfri används skriv i textrutan "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(274, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(190, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "med punkten först (ex .doc ))"
        '
        'NameItMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(615, 420)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Status)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.StartRename)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "NameItMain"
        Me.Text = "SWECO NameIt"
        Me.TransparencyKey = System.Drawing.SystemColors.Info
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Status.ResumeLayout(False)
        Me.Status.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents XclsxFileName As System.Windows.Forms.TextBox
    Friend WithEvents RenameFileName As System.Windows.Forms.TextBox
    Friend WithEvents StartRename As System.Windows.Forms.Button
    Friend WithEvents OpenFileOName As System.Windows.Forms.Button
    Friend WithEvents OpenFileReName As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Status As System.Windows.Forms.GroupBox
    Friend WithEvents StatusBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents tb_ending As TextBox
    Friend WithEvents rb_Else As RadioButton
    Friend WithEvents rb_Pdf As RadioButton
End Class
