﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Sweco NameIt")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("Sweco Rail AB")>
<Assembly: AssemblyProduct("Sweco NameIt")>
<Assembly: AssemblyCopyright("Copyright © Sweco 2016")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("ba674293-afe4-4a69-839d-a4fe2af6839a")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.4.0.13")>
<Assembly: AssemblyFileVersion("1.4.0.13")>
