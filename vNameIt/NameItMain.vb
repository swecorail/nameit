﻿
Imports Excel = Microsoft.Office.Interop.Excel
Imports System
Imports System.IO
Imports System.IO.Path
Imports System.Text.RegularExpressions


Public Class NameItMain


    Dim ONameFileObject As OpenFileDialog = New OpenFileDialog
    Dim RenameFileObject As OpenFileDialog = New OpenFileDialog
    Private Sub OpenFileOName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenFileOName.Click


        ONameFileObject.Filter = "Excel files (*.xlsx)| *.xlsx| All files (*.*)| *.*"
        ONameFileObject.FilterIndex = 1

        ONameFileObject.Multiselect = False
        ONameFileObject.ShowDialog()

        XclsxFileName.Text = ONameFileObject.FileName

    End Sub

    Private Sub OpenFileReName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenFileReName.Click
        Dim InputFiles As String = ""
        Dim NrOfFiles As Integer
        Dim i As Integer

        StatusBox.Text = ""
        RenameFileObject.Filter = "All files (*.*)| *.*"
        RenameFileObject.FilterIndex = 0

        RenameFileObject.Multiselect = True
        RenameFileObject.ShowDialog()

        NrOfFiles = RenameFileObject.FileNames.Count
        If RenameFileObject.FileNames.Count = 0 Then
            GoTo EndOpenRename
        End If
        InputFiles = RenameFileObject.FileNames(0)
        For i = 1 To NrOfFiles - 1
            InputFiles = InputFiles & ";" & RenameFileObject.FileNames(i)
        Next
        StatusBox.Text = "Antalet filer att byta namn på: " & RenameFileObject.FileNames.Count
        RenameFileName.Text = InputFiles
EndOpenRename:
    End Sub

    Private Sub StartRename_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StartRename.Click
        Dim ExcelObject As Excel.Application
        Dim ExcelWorkbook As Excel.Workbook
        Dim ExcelWorkSheet As Excel.Worksheet
        Dim OrginalName As String
        Dim FilePathName As String
        Dim CurrentFileName As String
        Dim CurrentFileLenght As String = ""
        Dim OrginalLenght As String = ""
        Dim NewFileNameObject As OpenFileDialog = New OpenFileDialog
        Dim i As Integer = 0
        Dim dotRegEx As Regex = New Regex("[\.]")
        Dim underscoreRegEx = New Regex("[_]")
        Try
            If ONameFileObject.FileName = "" Then
                '("Det måste finnas en excelfil med filnamn")
                MessageBox.Show("Det måste finnas en excelfil med filnamn", "Saknad fil", MessageBoxButtons.OK)
                GoTo EndStartRename
            End If


            If RenameFileObject.FileNames.Count < 1 Then
                MessageBox.Show("Det måste finnas en fil att byta namn på", "Saknad fil", MessageBoxButtons.OK)
                GoTo EndStartRename
            End If

            ExcelObject = New Excel.Application
            ExcelWorkbook = ExcelObject.Workbooks.Open(ONameFileObject.FileName)
            ExcelWorkSheet = ExcelWorkbook.Worksheets(1) ' Ibland heter det Sheet1 och ibland heter bladet Blad1 därför kör hårt med 1 

            For i = 0 To RenameFileObject.FileNames.Count - 1
                If File.Exists(RenameFileObject.FileNames(i)) = False Then
                    MessageBox.Show(RenameFileObject.FileNames(i) & " saknas, programmet redan kört med denna fil", "fil saknas", MessageBoxButtons.OK)
                    GoTo CloseExcel
                End If

                CurrentFileName = Path.GetFileName(RenameFileObject.FileNames(i))

                FilePathName = Path.GetDirectoryName(RenameFileObject.FileNames(i))

                OrginalName = ExcelWorkSheet.Cells(i + 2, 1).value
                If OrginalName = "" Then
                    MessageBox.Show("Antalet poster i Excelarket är färre än antalet filer som skall namnges", "missmatch i Excel", MessageBoxButtons.OK)
                    GoTo CloseExcel
                End If

                'While OrginalName.Length > 1 ' filtyp måste minst vara ett tecken
                '    OrginalLenght = FindLastUnderscore(OrginalName)
                'End While

                ' vi gör ett antagande att excel filen är skapad av IDA och IDA byter ut . till _ 
                OrginalName = ExcelWorkSheet.Cells(i + 2, 1).value ' hela filnamnet måste finnas i variabeln för att sedan ersätta sista _ med . 

                ' kolla om det finns en "."
                If dotRegEx.IsMatch(OrginalName) Then
                    Dim match As MatchCollection = dotRegEx.Matches(OrginalName)
                    Dim index As Integer = match.Item(match.Count - 1).Index
                    OrginalName = OrginalName.Remove(index, OrginalName.Length - index)
                Else
                    Dim testvariable = OrginalName.ToLower
                    If testvariable.Contains("pdf") Then
                        Dim match As MatchCollection = underscoreRegEx.Matches(OrginalName)
                        Dim index As Integer = match.Item(match.Count - 1).Index
                        OrginalName = OrginalName.Remove(index, OrginalName.Length - index)
                    End If
                   
                End If
                'OrginalName = OrginalName.Remove(OrginalName.Length - OrginalLenght.Length - 1, OrginalLenght.Length + 1)
                'If UsePDF.Checked Then ' Om orginalfilen är en .xls fil som har scannats så skall vi också byta endelse till .pdf eftersom att det är en pdf fil.
                'OrginalName = OrginalName & "." & OrginalLenght
                'Else
                If rb_Else.Checked = False Then
                    OrginalName = OrginalName & ".pdf" ' jag kan ju lika väl alltid välja .pdf själv
                Else
                    OrginalName = OrginalName & tb_ending.Text
                End If

                'End If
                NewFileNameObject.FileName = FilePathName & "\" & OrginalName
                If File.Exists(NewFileNameObject.FileName) Then
                    File.Delete(NewFileNameObject.FileName)
                    StatusBox.Text = StatusBox.Text & Environment.NewLine & NewFileNameObject.FileName & " fanns redan och togs bort"
                End If

                Rename(RenameFileObject.FileNames(i), NewFileNameObject.FileName)
                StatusBox.Text = StatusBox.Text & Environment.NewLine & "Byter namn på " & RenameFileObject.FileNames(i)

            Next
CloseExcel:
            ExcelWorkbook.Close()
            ExcelObject.Quit()
            StatusBox.Text = StatusBox.Text & Environment.NewLine & "Klar."
EndStartRename:
        Catch ex As Exception
            MessageBox.Show("Fel uppstod: " + ex.Message, "Felmeddelande", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub
    Private Function FindLast(ByRef rad As String)
        Dim pos As Integer
        pos = InStr(1, rad, "\")
        If pos > 0 Then
            FindLast = Trim(Mid(rad, 1, pos))
            rad = Trim(Mid(rad, pos + 1, rad.Length))
        Else
            FindLast = rad
            rad = ""
        End If
    End Function

    Private Function FindLastUnderscore(ByRef rad As String)
        Dim pos As Integer
        pos = InStr(1, rad, "_")
        If pos > 0 Then
            FindLastUnderscore = Trim(Mid(rad, 1, pos))
            rad = Trim(Mid(rad, pos + 1, rad.Length))
        Else
            FindLastUnderscore = rad
            rad = ""
        End If
    End Function

    Private Sub rb_Else_CheckedChanged(sender As Object, e As EventArgs) Handles rb_Else.CheckedChanged
        tb_ending.Enabled = True

    End Sub

    Private Sub rb_Pdf_CheckedChanged(sender As Object, e As EventArgs) Handles rb_Pdf.CheckedChanged
        tb_ending.Enabled = False
    End Sub
End Class

